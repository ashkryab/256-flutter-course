##Lesson 3
####Navigation
Учимся делать [переходы](https://flutter.dev/docs/cookbook/navigation/navigation-basics "переходы") между экранами, и [передавать данные](https://flutter.dev/docs/cookbook/navigation/passing-data "передавать данные") с одного в другой и [обратно](https://flutter.dev/docs/cookbook/navigation/returning-data "обратно"). Еще есть вариант для людей которые пришли с веба ([жмяк](https://flutter.dev/docs/cookbook/navigation/named-routes "жмяк")),([жмяк](https://flutter.dev/docs/cookbook/navigation/navigate-with-arguments "жмяк"))

Смотрим пример красивой и простой анимации ([видео](https://www.youtube.com/watch?v=Be9UH1kXFDw "видео")) ([дока](https://flutter.dev/docs/cookbook/navigation/hero-animations "дока"))

Дополнительное чтиво для тех кто проводит много времени в соц сетях. Обратите внимание что Navigator это StatefulWidget, вопрос, почему\зачем? Еще вопрос, как же все таки его заюзать в роле виджета, и какие кейсы это поможет решить?

Задача : Продолжаем работать над интернет магазином. По нажатию на товар, попадаем на экран с его детальным описанием, где его можно дабвить в корзину, в каком-то количестве. Не использовать статические переменные и функции, передавать между экранами. Добавляем екран Корзины, где отображаються все добавленные товары, которые можно удалить.
Корзину рисуем в [AppBar](https://api.flutter.dev/flutter/material/AppBar-class.html "AppBar") который лежит в [Scaffold](http://https://api.flutter.dev/flutter/material/Scaffold-class.html "Scaffold"), про последний тоже читаем подробно, используем как корневой виджет на всех экранах
