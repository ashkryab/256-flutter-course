###Iintroduce to Flutter 
В этой домашке вы рассмотрите основные элементы верстки и позиционирования виджетов

- [Text](https://api.flutter.dev/flutter/widgets/Text-class.html "Text"), [Image.assets](https://api.flutter.dev/flutter/widgets/Image/Image.asset.html "Image.assets")
- [Material](https://api.flutter.dev/flutter/material/Material-class.html "Material") (elevation)
- [Row](https://api.flutter.dev/flutter/widgets/Row-class.html "Row"), [Column](https://api.flutter.dev/flutter/widgets/Column-class.html "Column"), [Stack](https://api.flutter.dev/flutter/widgets/Stack-class.html "Stack") ([видео](https://www.youtube.com/watch?v=liEGSeD3Zt8&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG&index=50 "видео"))
- [Center](https://api.flutter.dev/flutter/widgets/Center-class.html "Center")
- [Container](https://api.flutter.dev/flutter/widgets/Container-class.html "Container") (Border, Shadow),
- Padding, Align ([видео](https://www.youtube.com/watch?v=g2E7yl3MwMk&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG&index=26 "видео"))
- [Expanded (видео)](https://api.flutter.dev/flutter/widgets/SingleChildScrollView-class.html "Expanded (видео)")
- [SingleChildScrollView](https://api.flutter.dev/flutter/widgets/SingleChildScrollView-class.html "SingleChildScrollView")

Если заюзаете еще какие виджеты будет только лучше. Помните что контейнер это один из основных виджетов, который умеет задавать размер (особенно если ругаеться что виджет бесконечен в ширину или высоту), тень, бордеры, закруглять углы, градиент и многое другое...
Expanded можно ложить только в виджеты которые наследуют Flex. Виджеты з скоролом не любят когда их бесконечно растягивают (Column c Expanded внутри), для этого колонну нужно ограничить по размеру, или выбрать другой костыль.

Картинки ложим в, ручками созданую, в корне проекта, папку с именем "assets", не забываем[ указать ее в pubspec.yaml](https://flutter.dev/docs/development/ui/assets-and-images " указать ее в pubspec.yaml")

Задание: верстаем красивый дизайн в StatelessWidget, без интерактива...совсем без, только статика. Чем больше рюшечек и декораций у виджетов тем лучше, текст разного размера, цвета и жирности, тень, бордеры и закругления...
